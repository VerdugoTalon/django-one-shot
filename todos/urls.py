from django.urls import path
from .views import show_detail, show_todos, create_todo_list, edit_todo_list, delete_todo_list, create_todo_item, update_todo_item

urlpatterns = [
    path('', show_todos, name='todo_list_list'),
    path('<int:id>/', show_detail, name='todo_list_detail'),
    path('create/', create_todo_list, name='todo_list_create'),
    path('<int:id>/edit/', edit_todo_list, name='todo_list_update'),
    path('<int:id>/delete/', delete_todo_list, name='todo_list_delete'),
    path('items/create/', create_todo_item, name='todo_item_create'),
    path('items/<int:id>/edit/', update_todo_item, name='todo_item_update'),
]